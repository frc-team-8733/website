#!/bin/bash

cd html
for f in *.html; do
	# do some stuff here with "$f"
	# remember to quote it or spaces may misbehave
	../tools/includes.sh "$f" > "../public/$f"
done

