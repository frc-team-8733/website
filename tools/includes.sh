#!/bin/bash


while IFS="" read -r p || [ -n "$p" ]
do
  printf '%s\n' "$p"
  if ( echo "$p" | grep -e '<!--include' > /dev/null )
  then
	cat "../includes/$(echo "$p" | sed 's/<!--include file=//' | sed 's/-->//' | tr -d '"' | sed 's/^[[:space:]]*//g' )"
  fi
done < $1
