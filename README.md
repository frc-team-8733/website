This is the website for the Athens Renninsance Robotics Team, FRC Team 8733.

This is a static site served by cloudflare pages at https://ars-frc.pages.dev/. This site uses staticly generated HTML includes. This is done by the `tools/includes.sh` script which is called by `tools/foreachfile.sh` which runs the script on each html file in `html/` and sends the output to the corosponding file in `public/`.
